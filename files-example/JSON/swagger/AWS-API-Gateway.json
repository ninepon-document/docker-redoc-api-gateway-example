{
  "swagger": "3.0.3",
  "info": {
    "version": "2023-03-20T10:58:57Z",
    "title": "API Gateway NINEPON service (develop)",
    "contact": {
      "name": "software developer",
      "url": "https://www.ninepon.com/"
    },
    "x-logo": {
      "url": "https://ninepon.com/logo.gif",
      "altText": "ninepon",
      "href": "https://ninepon.com/"
    },
    "description": "# Introduction\nninepon API is built on HTTP and is RESTful. \n# Authentication\nWe will have 2 keys to authorize in our system by attaching them to the header.\n\n- x-api-key: Use to make requests from the AWS APi Gateway. Never share these keys. Keep them guarded and secure.\n- Authorization: Use in your authorize information.\n<!-- ReDoc-Inject: <security-definitions> -->\n"
  },
  "x-servers": [
    {
      "url": "https://api.ninepon.com/v1",
      "description": "API Gateway version 1"
    }
  ],
  "x-server-multi": true,
  "x-server-selector": true,
  "schemes": [
    "https"
  ],
  "paths": {
    "/machine_slots/{machine_id}": {
      "get": {
        "tags": [
          "Machine slots"
        ],
        "description": "Get machine slots",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "machine_id",
            "in": "path",
            "required": true,
            "type": "string",
            "description": "Machine Id (example: '000009')"
          }
        ],
        "responses": {
          "207": {
            "description": "Sample first slot from machine (In fact, There are more than 1 slot)",
            "schema": {
              "$ref": "#/definitions/ResponseGetMachineSlotsSuccess"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      },
      "/machine_slots": null,
      "put": {
        "tags": [
          "Machine slots"
        ],
        "summary": "Update machine slots",
        "description": "In this document have 4 event occour to slots <br>1. Change product on slot<br>2. Change slot info (enable, selling price, max quantity)<br>3. Close or disable slot<br>4. action with quantity on slot",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Body update machine slots",
            "description": "In this document have 4 samples",
            "schema": {
              "type": "object",
              "properties": {
                "machine_id": {
                  "type": "string",
                  "required": true,
                  "description": "Machine id (example: '000009')"
                },
                "datetime": {
                  "type": "string",
                  "required": true,
                  "description": "Date time currently format 'YYYY-MM-dd HH:mm:ss' (example: '2021-08-19 13:20:0')"
                },
                "data_slots": {
                  "type": "array",
                  "required": true,
                  "description": "Array multiple slots that are your need to change",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "slot": {
                          "type": "integer",
                          "format": "int32",
                          "required": true,
                          "description": "Slot number (example: 1)"
                        },
                        "enable_slot": {
                          "type": "string",
                          "enum": [
                            "Y",
                            "N"
                          ],
                          "description": "Enable this slot via 'Y'=enable, 'N'=disable (example: 'Y')"
                        },
                        "product_code": {
                          "type": "string",
                          "description": "Change product in this slot via product code (example: '9900')"
                        },
                        "selling_price": {
                          "type": "integer",
                          "format": "int32",
                          "description": "Change selling price in this slot (example: 10)"
                        },
                        "max_quantity": {
                          "type": "integer",
                          "format": "int32",
                          "description": "Change maximun quantity in this slot (example: 20)"
                        },
                        "quantity": {
                          "type": "integer",
                          "format": "int32",
                          "description": "Change quantity in this slot, but must below or equal than max quantity (example: 20)"
                        },
                        "act": {
                          "type": "string",
                          "enum": [
                            "add",
                            "del",
                            "fixed"
                          ],
                          "description": "'add'=increase the quantity from the value of the quantity to the original item, <br>'del'=decrease the quantity from the value of the quantity to the original item, <br>'fixed'=fixed value from quantity to in this slot <br>(example: 'add')"
                        }
                      }
                    }
                  ]
                }
              }
            },
            "in": "body",
            "x-examples": {
              "application/json": {
                "machine_id": "000009",
                "datetime": "2021-08-19 13:20:0",
                "data_slots": [
                  {
                    "slot": 1,
                    "enable_slot": "Y",
                    "product_code": "123456",
                    "selling_price": 200,
                    "max_quantity": 50,
                    "quantity": 15,
                    "act": "add"
                  },
                  {
                    "slot": 2,
                    "enable_slot": "N",
                    "product_code": "-",
                    "selling_price": 0,
                    "max_quantity": 0
                  },
                  {
                    "slot": 3,
                    "enable_slot": "Y",
                    "product_code": "123456",
                    "selling_price": 200,
                    "max_quantity": 50
                  },
                  {
                    "slot": 4,
                    "quantity": 20,
                    "act": "fixed"
                  }
                ]
              }
            }
          }
        ],
        "responses": {
          "200": {
            "x-expand": true,
            "x-expand-schema": true,
            "description": "response",
            "schema": {
              "$ref": "#/definitions/ResponseUpdateMachineSlotsMultipleResult"
            }
          },
          "404": {
            "x-expand": true,
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      }
    },
    "/product": {
      "post": {
        "tags": [
          "Product"
        ],
        "description": "Add product",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Content-Type",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "application/json"
          },
          {
            "name": "Body",
            "description": "Add new prodcut, Body type is multipart/form-data",
            "consumes": [
              "multipart/form-data"
            ],
            "schema": {
              "type": "object",
              "properties": {
                "code": {
                  "type": "string",
                  "required": true,
                  "description": "Product code"
                },
                "name": {
                  "type": "string",
                  "required": true,
                  "description": "Product name"
                },
                "price": {
                  "type": "integer",
                  "format": "int32",
                  "required": true,
                  "description": "Product price"
                },
                "type": {
                  "type": "string",
                  "required": true,
                  "description": "Product type"
                },
                "detail": {
                  "type": "string",
                  "required": true,
                  "description": "Product detail"
                },
                "image": {
                  "type": "string",
                  "required": false,
                  "description": "Product image"
                },
                "video": {
                  "type": "string",
                  "required": false,
                  "description": "Product video"
                },
                "company_id": {
                  "type": "integer",
                  "format": "int32",
                  "required": false,
                  "description": "your company id"
                }
              }
            },
            "in": "body",
            "x-examples": {
              "application/json": {
                "code": "99906",
                "name": "testbox",
                "price": 10,
                "type": "BOX",
                "detail": "testbox testbox",
                "image": "<image>",
                "video": "<video>",
                "company_id": 6
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Create product has Successful",
            "schema": {
              "$ref": "#/definitions/ResponsePostAndPutProductSuccess"
            }
          },
          "409": {
            "x-expand": true,
            "description": "Conflict (duplicate product code)",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "422": {
            "x-expand": true,
            "description": "Unprocessable Entity (values in the body are not complete)",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      },
      "put": {
        "tags": [
          "Product"
        ],
        "description": "Update product",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Content-Type",
            "in": "header",
            "required": true,
            "type": "string",
            "description": "application/json"
          },
          {
            "name": "Body",
            "description": "Add new prodcut, Body type is multipart/form-data",
            "consumes": [
              "multipart/form-data"
            ],
            "schema": {
              "type": "object",
              "properties": {
                "code": {
                  "type": "string",
                  "required": true,
                  "description": "Product code"
                },
                "name": {
                  "type": "string",
                  "required": true,
                  "description": "Product name"
                },
                "price": {
                  "type": "integer",
                  "format": "int32",
                  "required": true,
                  "description": "Product price"
                },
                "type": {
                  "type": "string",
                  "required": true,
                  "description": "Product type"
                },
                "detail": {
                  "type": "string",
                  "required": true,
                  "description": "Product detail"
                },
                "image": {
                  "type": "string",
                  "required": false,
                  "description": "Product image"
                },
                "video": {
                  "type": "string",
                  "required": false,
                  "description": "Product video"
                },
                "company_id": {
                  "type": "integer",
                  "format": "int32",
                  "required": false,
                  "description": "your company id"
                }
              }
            },
            "in": "body",
            "x-examples": {
              "application/json": {
                "code": "99906",
                "name": "testbox",
                "price": 10,
                "type": "BOX",
                "detail": "testbox testbox",
                "image": "<image>",
                "video": "<video>",
                "company_id": 6
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Update product has successful",
            "schema": {
              "$ref": "#/definitions/ResponsePostAndPutProductSuccess"
            }
          },
          "404": {
            "x-expand": true,
            "description": "Product not Found",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "422": {
            "x-expand": true,
            "description": "Unprocessable Entity (values in the body are not complete)",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      }
    },
    "/product/{product_code}": {
      "get": {
        "tags": [
          "Product"
        ],
        "description": "Get product via product code",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "product_code",
            "in": "path",
            "required": true,
            "type": "string",
            "description": "product code (example: 990000)"
          }
        ],
        "responses": {
          "200": {
            "x-expand": true,
            "x-expand-schema": true,
            "description": "Get product has successful",
            "schema": {
              "$ref": "#/definitions/ResponseGetProductSuccess"
            }
          },
          "404": {
            "x-expand": true,
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      },
      "delete": {
        "tags": [
          "Product"
        ],
        "description": "Delete product via product code",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "product_code",
            "in": "path",
            "required": true,
            "type": "string",
            "description": "product code (example: 990000)"
          }
        ],
        "responses": {
          "200": {
            "description": "Delete product has success",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          },
          "404": {
            "x-expand": true,
            "description": "Not Found",
            "schema": {
              "$ref": "#/definitions/Empty"
            }
          }
        },
        "security": [
          {
            "api_key": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "api_key": {
      "type": "apiKey",
      "name": "x-api-key",
      "in": "header"
    },
    "svms_auth": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  },
  "components": {
    "headers": {
      "x-api-key": {
        "name": "x-api-key",
        "in": "header",
        "required": true,
        "type": "string"
      },
      "Authorization": {
        "name": "Authorization",
        "in": "header",
        "required": true,
        "type": "string"
      }
    },
    "schemas": {
      "Forbidden": null,
      "type": "object",
      "properties": {
        "message": "string"
      },
      "example": [
        {
          "message": "Authentication failed on SVMS API service"
        }
      ]
    },
    "responses": {
      "Forbidden": {
        "description": "Access forbidden.",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "#/components/schemas/Forbidden"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Empty": {
      "type": "object",
      "title": "Empty Schema"
    },
    "ResponseUpdateMachineSlotsMultipleResult": {
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "machine_id": {
              "type": "string",
              "description": "machine id"
            },
            "slot": {
              "type": "integer",
              "description": "slot machine"
            },
            "result": {
              "type": "boolean",
              "description": "result of slot from your request"
            },
            "message": {
              "type": "string",
              "description": "describe of response"
            }
          }
        }
      ],
      "example": [
        {
          "machine_id": "000009",
          "slot": 11,
          "result": false,
          "message": "max quantity (50) must more then quantity (15)"
        },
        {
          "machine_id": "000009",
          "slot": 2,
          "result": true,
          "message": "successed in case clear slot"
        }
      ]
    },
    "ResponseGetMachineSlotsSuccess": {
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "1": {
              "type": "object",
              "properties": {
                "company_id": {
                  "type": "integer",
                  "format": "int32",
                  "description": "company id"
                },
                "machine_id": {
                  "type": "string",
                  "description": "machine id"
                },
                "slot": {
                  "type": "integer",
                  "format": "int32",
                  "description": "slot machine"
                },
                "product_desc": {
                  "type": "string",
                  "description": "product description"
                },
                "enable_slot": {
                  "type": "boolean",
                  "description": "product enable or disable"
                },
                "max_quantity": {
                  "type": "integer",
                  "format": "int32",
                  "description": "max quantity of this slot"
                },
                "product_video": {
                  "type": "string",
                  "description": "product video"
                },
                "product_code": {
                  "type": "string",
                  "description": "product code"
                },
                "product_image": {
                  "type": "string",
                  "description": "product image"
                },
                "product_lang": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "th": {
                        "type": "string",
                        "description": "product language thai"
                      },
                      "en": {
                        "type": "string",
                        "description": "product language enaglish"
                      }
                    }
                  }
                },
                "product_type": {
                  "type": "string",
                  "description": "product type"
                },
                "product_name": {
                  "type": "string",
                  "description": "product name"
                },
                "product_price": {
                  "type": "integer",
                  "format": "int32",
                  "description": "product price"
                },
                "quantity": {
                  "type": "integer",
                  "format": "int32",
                  "description": "quantity of product in slot"
                },
                "selling_price": {
                  "type": "integer",
                  "format": "int32",
                  "description": "selling price of product in slot"
                },
                "set_slot": {
                  "type": "object",
                  "properties": {
                    "fixed": {
                      "type": "integer",
                      "format": "int32",
                      "description": "quantity action fixed"
                    },
                    "del": {
                      "type": "integer",
                      "format": "int32",
                      "description": "quantity action delete"
                    },
                    "add": {
                      "type": "integer",
                      "format": "int32",
                      "description": "quantity action delete"
                    },
                    "last_act_success": {
                      "type": "boolean",
                      "description": "machine pickup set_slot to update machine slot"
                    },
                    "updated_at": {
                      "type": "string",
                      "description": "date time machine pcikup action to machine"
                    }
                  }
                }
              }
            }
          }
        }
      ],
      "example": [
        {
          "1": {
            "company_id": 6,
            "machine_id": "000009",
            "slot": 1,
            "product_desc": "",
            "enable_slot": true,
            "max_quantity": 50,
            "product_video": "https://s3.ap-southeast-1.amazonaws.com/ninepon/product/video-tms-201300.mp4",
            "product_code": "123456",
            "product_image": "https://s3.ap-southeast-1.amazonaws.com/ninepon/product/123456.jpg",
            "product_lang": [
              {
                "th": "กดเกดเกดเกดเ"
              },
              {
                "en": "fghfghfgh"
              }
            ],
            "product_type": "GG",
            "product_name": "กดเกดเกดเกดเ",
            "product_price": 1,
            "quantity": 2,
            "selling_price": 200,
            "set_slot": {
              "fixed": 0,
              "del": 0,
              "add": 15,
              "last_act_success": false,
              "updated_at": "2023-03-16T09:38:17.058Z"
            }
          }
        }
      ]
    },
    "ResponseGetProductSuccess": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "product code"
        },
        "company_id": {
          "type": "number",
          "description": "company id"
        },
        "name_lang": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "th": {
                "type": "string",
                "description": "product language thai"
              },
              "en": {
                "type": "string",
                "description": "product language thai"
              }
            }
          }
        },
        "created_at": {
          "type": "string",
          "description": "date time create product"
        },
        "video": {
          "type": "object",
          "properties": null,
          "description": "video of product in this slot"
        },
        "type": {
          "type": "string",
          "description": "type of product"
        },
        "price": {
          "type": "number",
          "description": "price of product"
        },
        "name": {
          "type": "string",
          "description": "name thai product"
        },
        "detail": {
          "type": "string",
          "description": "detail of product"
        },
        "name_en": {
          "type": "string",
          "description": "name enlish of product"
        },
        "image": {
          "type": "string",
          "description": "image of product in this slot"
        },
        "updated_at": {
          "type": "string",
          "description": "date time last update product"
        }
      },
      "example": {
        "code": "999008",
        "company_id": 6,
        "name_lang": [
          {
            "th": "newproduct1"
          },
          {
            "en": "newproduct1"
          }
        ],
        "created_at": "2022-09-17T05:29:51.221Z",
        "video": null,
        "type": "CAN",
        "price": 11,
        "name": "newproduct1",
        "detail": "",
        "name_en": "newproduct1",
        "image": "https://s3.ap-southeast-1.amazonaws.com/ninepon/product/image-tms1678972106786-999008.png",
        "updated_at": "2023-03-16T13:08:27.262Z"
      }
    },
    "ResponsePostAndPutProductSuccess": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "code of  product"
        },
        "company_id": {
          "type": "number",
          "description": "company id"
        },
        "name_lang": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "th": {
                "type": "string",
                "description": "product language thai"
              },
              "en": {
                "type": "string",
                "description": "product language thai"
              }
            }
          }
        },
        "created_at": {
          "type": "string",
          "description": "date time create product"
        },
        "video": {
          "type": "object",
          "properties": null,
          "description": "video of product in this slot"
        },
        "type": {
          "type": "string",
          "description": "type of product"
        },
        "price": {
          "type": "number",
          "description": "price of product"
        },
        "name": {
          "type": "string",
          "description": "name thai of product"
        },
        "detail": {
          "type": "string",
          "description": "detail of product"
        },
        "name_en": {
          "type": "string",
          "description": "name english of product"
        },
        "image": {
          "type": "string",
          "description": "image of product in this slot"
        },
        "updated_at": {
          "type": "string",
          "description": "date time last update product"
        }
      },
      "example": {
        "code": "999008",
        "company_id": 6,
        "name_lang": [
          {
            "th": "newproduct1"
          },
          {
            "en": "newproduct1"
          }
        ],
        "created_at": "2022-09-17T05:29:51.221Z",
        "video": null,
        "type": "CAN",
        "price": 11,
        "name": "newproduct1",
        "detail": "",
        "name_en": "newproduct1",
        "image": "https://s3.ap-southeast-1.amazonaws.com/ninepon/product/image-tms1678972106786-999008.png",
        "updated_at": "2023-03-16T13:08:27.262Z"
      }
    }
  }
}