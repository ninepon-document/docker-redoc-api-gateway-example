# Use an official Node.js runtime as a parent image
FROM node:14-alpine

ENV VARIABLE_DOC_FILE init-value.json

# Set the working directory to /app
WORKDIR /app

# Install any dependencies listed in the package.json file
RUN npm install http-server -g

# Copy the HTML file to the container
COPY index.html .
COPY edit_index.sh .
# RUN sed -i 's#<redoc spec-url="file.json">#<redoc spec-url="'"$VARIABLE_DOC_FILE"'">#' index.html

# Expose port 8080 for the application
EXPOSE 8080

# Start the http-server on container startup
CMD ["http-server", "-p", "8080"]